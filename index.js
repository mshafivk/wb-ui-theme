/* jshint node: true */
'use strict';

module.exports = {
  name: 'wb-ui-theme',
  included: function(app) {
  	console.log("============"+app)
   app.import(app.bowerDirectory + '/bootstrap/dist/js/bootstrap.js');
   app.import(app.bowerDirectory + '/bootstrap/dist/css/bootstrap.css');
   this._super.included(app);
 }
};
