# Wb-ui-theme

This README outlines the details of collaborating on this Ember addon.
## Addon Information
This is an addon in Foundation Project
Addon contain base Sass styles -body or screen related, icon sets dependencies
Bootstrap and other css framework dependencies

## Installation

* `git clone` this repository
* remove node_modules tmp bower_components folders (which ever required)
   * command to remove dir in linux rm -rf node_modules/ tmp/ bower_components
* run `node setup-project.js`

* setup-project is node sript which will take care of installing dependencies (npm,bower and other addons related deps)
## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `npm test` (Runs `ember try:testall` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://www.ember-cli.com/](http://www.ember-cli.com/).
